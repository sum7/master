package com.poc.test.integration;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.poc.rest.config.WebMvcConfig;
import com.poc.rest.model.Person;

@ContextConfiguration(classes = { WebMvcConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AppControllerIntegrationTest {
   
	 private static RestTemplate restTemplate = null;
	 private static final String baseURL = "http://localhost:8026/ITWithMultiDB/person/";
	 private HttpEntity<Person> en1=null;
	 private HttpEntity<Person> en2=null;
	 private HttpEntity<Person> blankName=null;
	 
		
	 @Before
	 public void setup() {
		
		 	 
		 restTemplate=new RestTemplate();
		 en1=new HttpEntity<Person>(new Person(1,"Trying","CCC",4524));
		 en2=new HttpEntity<Person>(new Person(1,"TEST","IT",524));
		 blankName=new HttpEntity<Person>(new Person(0,"","CCC",4524));
		
	 }
	
	 	 	 
	@Test
	public void testPersonRequestsFlow()
	{
		//addPerson()
		ResponseEntity<Integer> resp=restTemplate.exchange(baseURL+"add", HttpMethod.POST, en1, Integer.class);
		 		assertThat(HttpStatus.OK,is(resp.getStatusCode()));
		
		
		 
		  //getAllPersons()
		 ResponseEntity<List> resp1=restTemplate.exchange(baseURL+"all", HttpMethod.GET, null, List.class); 
		   assertThat(HttpStatus.OK, is(resp1.getStatusCode()));
		 
		 
		  
		  //getPersonByUid() 
		   ResponseEntity<Person> resp2=restTemplate.exchange(baseURL+"/{uid}",HttpMethod.GET,null, Person.class,resp.getBody());
		  assertThat(HttpStatus.OK,is(resp2.getStatusCode()));
		 
		
		    
		        
		
		  //updatePerson()
		  ResponseEntity<Person> resp3=restTemplate.exchange(baseURL+"/{uid}",HttpMethod.PUT,en2,Person.class,resp.getBody()); 
		  assertThat(HttpStatus.OK,is(resp3.getStatusCode()));
		 
		  		        
		
		  //deletePerson() 
		
		  ResponseEntity<Integer> resp4=restTemplate.exchange(baseURL+"/{uid}",HttpMethod.DELETE,null,Integer.class,resp3.getBody().getUid());
		  assertThat(HttpStatus.OK,is(resp4.getStatusCode()));
		 
		 		 		     
		 
	}
	
	
	@Test(expected = HttpServerErrorException.class)
	public void negativeTestForBlankNameInsertion()
	{
		
		 ResponseEntity<Integer> resp6=restTemplate.exchange(baseURL+"add", HttpMethod.POST,blankName,Integer.class);
			  assertThat(HttpStatus.INTERNAL_SERVER_ERROR,is(resp6.getStatusCode()));
	}
	
	
	
	@Test(expected = HttpClientErrorException.class)
	public void negativeTestForGetPerson()
	{ 
		ResponseEntity<Person> resp5=restTemplate.exchange(baseURL+"/{uid}",HttpMethod.GET,null,Person.class,999999);
	    	  assertThat(HttpStatus.NOT_FOUND,is(resp5.getStatusCode()));
	}
		   
}
