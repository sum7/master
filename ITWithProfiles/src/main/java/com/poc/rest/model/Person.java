package com.poc.rest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int uid;
	
	private String name;
	private String city;
	private int pin;
	
	
	public Person()
	{}
		
	public Person(int uid,String name, String city, int pin) {
		super();
		this.uid=uid;
		this.name = name;
		this.city = city;
		this.pin = pin;
	}

		
	public int getUid() {
		return uid;
	}


	public void setUid(int uid) {
		this.uid = uid;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", city=" + city + ", pin=" + pin + "]";
	}
	
	
	
	
	
}
