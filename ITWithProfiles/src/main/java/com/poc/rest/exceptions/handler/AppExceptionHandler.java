package com.poc.rest.exceptions.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.poc.rest.exceptions.BlankInsertionException;
import com.poc.rest.exceptions.PersonNotFoundException;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	  private String message;
	  
	  public AppExceptionHandler()
	  { }
	  
	
	public AppExceptionHandler(String message)
	{
		this.message = message;
	}


	@ExceptionHandler(PersonNotFoundException.class)
	public final ResponseEntity<String> handlePersonNotfoundException()
	{
		
	  return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);	
	}
	
	
	@ExceptionHandler(BlankInsertionException.class)
	public final ResponseEntity<String> handleBlankNameException()
	{
		
	  return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);	
	}
	
}
