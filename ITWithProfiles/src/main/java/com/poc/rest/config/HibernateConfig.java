package com.poc.rest.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.poc.rest")
@PropertySource("classpath:database.properties")
public class HibernateConfig {

	
	@Autowired
	private Environment environment;
	
	@Bean("sessionFactory") 
	public LocalSessionFactoryBean sessionFactory(@Autowired DataSource dataSource )
	{
		LocalSessionFactoryBean factory=new LocalSessionFactoryBean();
		factory.setDataSource(dataSource);
        factory.setPackagesToScan(new String[] {"com.poc.rest"});	
		factory.setHibernateProperties(hibernateProperties());
				
		return factory;
	}

	

	@Bean("dataSource")
	@Profile("dev")
	public DataSource dataSource1() {
		DriverManagerDataSource dataSource=new  DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("database.driver"));
		dataSource.setUrl(environment.getRequiredProperty("database.url1"));
		dataSource.setUsername(environment.getRequiredProperty("database.username"));
		dataSource.setPassword(environment.getRequiredProperty("database.password"));
		
		return dataSource;
	}  
	
	
	@Bean("dataSource")
	@Profile("prod")
	public DataSource dataSource2() {
		DriverManagerDataSource dataSource=new  DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("database.driver"));
		dataSource.setUrl(environment.getRequiredProperty("database.url2"));
		dataSource.setUsername(environment.getRequiredProperty("database.username"));
		dataSource.setPassword(environment.getRequiredProperty("database.password"));
		
		return dataSource;
	}  
	

	@Bean
   public Properties hibernateProperties() {
		Properties properties=new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
		
	   return properties;	   	
	}
	

   @Bean
   public HibernateTransactionManager getTransactionManager(@Autowired SessionFactory sessionFactory) {
       HibernateTransactionManager transactionManager = new HibernateTransactionManager();
       transactionManager.setSessionFactory(sessionFactory);
       return transactionManager;
   }
   
   
   @Bean
   public HibernateTemplate getTemplate(@Autowired SessionFactory sessionFactory)
   {
	   HibernateTemplate template=new HibernateTemplate();
	   template.setSessionFactory(sessionFactory);
	    
	   return template;
   }
   
   
}
