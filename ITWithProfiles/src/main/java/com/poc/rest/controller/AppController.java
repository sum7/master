package com.poc.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poc.rest.exceptions.BlankInsertionException;
import com.poc.rest.exceptions.PersonNotFoundException;
import com.poc.rest.model.Person;
import com.poc.rest.service.AppService;

@RestController
@RequestMapping("/person")
public class AppController {
 
	@Autowired
	AppService appService;
	
	
	@GetMapping("/")
	 public String welcome()
	 {
		 return "Welcome!!";
	 }
	
	@GetMapping("/all")
	public List<Person> getAllPersons()
	{
		System.out.println("All persons");
		return appService.getAllPersons();
	}
	
	@PostMapping("/add")
	public int addPerson(@RequestBody Person person) throws BlankInsertionException  
	{
		System.out.println("In add person Method!!!");
		
		if(person.getName().isEmpty())
		{
			 throw new BlankInsertionException("Person Name Can not be blank");
		}
		
		return appService.addPerson(person);
	}
	
	@GetMapping("/{uid}")
	public Person getPerson(@PathVariable int uid) throws PersonNotFoundException,BlankInsertionException
 	{
		if(uid==0)
		{
		 throw new BlankInsertionException("Person UID Name Can not be 0");
		}
		Person p=appService.getPersonByUid(uid);
		
		if( p== null)
		{
			throw new PersonNotFoundException("No Entry Found with UID:"+uid);
		}
		
		return p; 
	}
	
	@PutMapping("/{uid}")
	public Person updatePerson(@PathVariable("uid") int uid,@RequestBody Person person) throws PersonNotFoundException,BlankInsertionException
	{
		if(uid==0)
		{
		 throw new BlankInsertionException("Person UID Can not be 0");
		}
		
		Person p=appService.updatePersonByUid(uid, person); 
		if(p == null)
		{
			throw new PersonNotFoundException("No Entry Found with UID:"+uid);
		}
		return p;
	}
	
	@DeleteMapping("/{uid}")
	public int deletePerson(@PathVariable("uid") int uid) throws PersonNotFoundException,BlankInsertionException
	{
		if(uid==0)
		{
		 throw new BlankInsertionException("Person UID Can not be 0");
		}
		
		int u=appService.deletePersonByUid(uid); 
		if(u == 0) 
		{
			throw new PersonNotFoundException("No Entry Found with UID:"+uid);
		}
		return uid;
		
	}
}
