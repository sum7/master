package com.poc.rest.service;

import java.util.List;
import java.util.Map;

import com.poc.rest.model.Person;

public interface AppService {

	public int addPerson(Person person);
	public Person getPersonByUid(int uid);
	public List<Person> getAllPersons();
	public Person updatePersonByUid(int uid,Person person);
	public int deletePersonByUid(int uid);
	
}
