package com.poc.rest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.rest.dao.AppDao;
import com.poc.rest.model.Person;
import com.poc.rest.service.AppService;

@Service
public class AppServiceImpl implements AppService {

	@Autowired
	private AppDao appDao;
	
	@Override
	public int addPerson(Person person) {
		
		return appDao.addPerson(person); 
	}

	@Override
	public Person getPersonByUid(int uid) {
		
		return appDao.getPersonByUid(uid); 
	}

	@Override
	public List<Person> getAllPersons() {
		
		return appDao.getAllPersons();
	}

	@Override
	public Person updatePersonByUid(int uid,Person person) {
		
		return appDao.updatePersonByUid(uid, person); 
	}

	@Override
	public int deletePersonByUid(int uid) {
	
		return appDao.deletePersonByUid(uid); 
	}

	

}
