package com.poc.rest.aop;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AppLogingAspect {

	Logger log = Logger.getLogger(getClass().getName());
	
		
	@Pointcut("execution(public * com.poc.rest.controller.*.*(..))")
	private void onlyForController()  {
		
	}

	@Before("onlyForController()")
	public void beforeLogAll( JoinPoint joinPoint)throws Exception {
		
			
		log.info("**Method Name:"+joinPoint.getSignature());
		
		
	}

	@AfterThrowing(pointcut = "onlyForController()",throwing = "ex")
	public void logException(Exception ex)
	{
		log.info("***Exception Caught:"+ex); 
	}
	
	
	
	
	
	
	
}
