package com.poc.rest.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.poc.rest.dao.AppDao;
import com.poc.rest.model.Person;

@Repository
public class AppDaoImpl implements AppDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
		
	@Override
	public int addPerson(Person person) {
		
		Session session=null;  
	    Transaction tx=null;
	    int uid;
	    
	    try 
	    {
	    	 session=sessionFactory.openSession();
	         tx=session.beginTransaction();
	         Person pr=session.get(Person.class, session.save(person)); 
	         uid=pr.getUid();
		    tx.commit();
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in Saving!!!!");
	      if(tx != null)
	      {
	    	  tx.rollback();
	    	  
	      }
	     throw e;
	    }
	    finally {
	    	if(session!=null)
			  {
	    		session.close();
			   }
		}	    
	     
	    return uid;
		
	}
	
	@Override
	public Person getPersonByUid(int uid) {
		
		Session session=null;  
	    Transaction tx=null;
	    Person person=null;
	    try 
	    {
	    	 session=sessionFactory.openSession();
	         tx=session.beginTransaction();
	         person=session.get(Person.class, uid);
		    tx.commit();
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in Getting Data !!!!");
	      if(tx != null)
	      {
	    	  tx.rollback();
	    	  
	      }
	     throw e;
	    }
	    finally {
	    	if(session!=null)
			  {
	    		session.close();
			   }
		}	    
	    
	    
		return person;  
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Person> getAllPersons() {
		
		Session session=null;  
	    Transaction tx=null;
	    List<Person> person=null;
	    try 
	    {
	    	 session=sessionFactory.openSession();
	         tx=session.beginTransaction();
	         Query<Person> query=session.createQuery("FROM Person");
	         person=query.list();
		    tx.commit();
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in Getting Data !!!!");
	      if(tx != null)
	      {
	    	  tx.rollback();
	    	  
	      }
	     throw e;
	    }
	    finally {
	    	if(session!=null)
			  {
	    		session.close();
			   }
		}	    
	    		
		return person;
	}

	@Override
	public Person updatePersonByUid(int uid, Person person) {
		Session session=null;  
	    Transaction tx=null;
	   
	    try 
	    {
	    	 session=sessionFactory.openSession();
	         tx=session.beginTransaction();
	         
	          session.delete("Person",session.get(Person.class, uid)); 
	          session.save(person);
	         tx.commit();
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in Updating !!!!");
	      if(tx != null)
	      {
	    	  tx.rollback();
	    	  
	      }
	      throw e;
	    }
	    finally {
	    	if(session!=null)
			  {
	    		session.close();
			   }
		}	    
	    		
		return person;
	}

	@Override
	public int deletePersonByUid(int uid) {

		Session session=null;  
	    Transaction tx=null;
	    try 
	    {
	    	 session=sessionFactory.openSession();
	         tx=session.beginTransaction();
	        session.delete("Person",session.get(Person.class, uid));
	          tx.commit();
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in Deleting!!!!");
	      if(tx != null)
	      {
	    	  tx.rollback();
	    	  
	      }
	     throw e;
	    }
	    finally {
	    	if(session!=null)
			  {
	    		session.close();
			   }
		}	    
	     
	    return uid;
	    }

	
	

}	
