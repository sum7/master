package com.poc.rest.dao;

import java.util.List;

import com.poc.rest.model.Person;

public interface AppDao {

	public int addPerson(Person person);
	public Person getPersonByUid(int uid);
	public List<Person> getAllPersons();
	public Person updatePersonByUid(int uid,Person person);
	public int deletePersonByUid(int uid);
		
	
	
}
